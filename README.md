# Autoregressive Time Series Observed Under classification (classified﻿ARp)

## General info:

**Language**: *Python* (computations), *R* (visualization), *TeX* (description)

**Author**: Hanna V. Rudakouskaya, Department of Mathematical Modelling and Data Analysis, Faculty of Applied Mathematics and Computer Science, Belarusian State University

**Scientific Supervisor**: Yuriy S. Kharin, Doctor of Physico-Mathematical Sciences, Full Professor, Associate Member of National Academy of Sciences of Belarus

Further hints can be found [in wiki](https://bitbucket.org/hannarud/classifiedarp/wiki/Home)

