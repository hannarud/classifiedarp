from nose.tools import *
from ARp.ARp import ARp
from ClassARp.ClassARp import ClassARp
import random


def test_init():
    # It's OK
    ClassARp(ARp([0.5], 4, 0, 1), [-float("inf"), 0])


def test_getitem():
    # Must be OK
    random.seed(1)
    y = ClassARp(ARp([0.5], 4, 0, 1), [-float("inf"), 0])
    assert_equal(y[1], 1)  # Because x[1] = 2.09 which is greater than 0

    # Getting non-existing 8th item
    assert_raises(IndexError, y.__getitem__, 8)


def test_length():
    assert_equal(len(ClassARp(ARp([0.5], 4, 0, 1), [-float("inf"), 0])), 4)


def test_str():
    print ClassARp(ARp([0.5], 4, 0, 1), [-float("inf"), 0])


def test_repr():
    repr(ClassARp(ARp([0.5], 4, 0, 1), [-float("inf"), 0]))
