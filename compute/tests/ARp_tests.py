from nose.tools import *
from ARp.ARp import ARp
import random


def test_init():
    # It's OK
    ARp([0.5], 4, 0, 1)
    
    # Not stationery, negative test
    assert_raises(AssertionError, ARp, [1.5], 4, 0, 1)


def test_getitem():
    # Must be OK
    random.seed(1)
    x = ARp([0.5], 4, 0, 1)
    assert_equal(x[1], 2.0935379852775022)

    # Getting non-existing 8th item
    assert_raises(IndexError, x.__getitem__, 8)


def test_length():
    assert_equal(len(ARp([0.5], 4, 0, 1)), 4)


def test_str():
    print ARp([0.5], 4, 0, 1)


def test_repr():
    repr(ARp([0.5], 4, 0, 1))
