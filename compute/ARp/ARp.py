import random


class ARp(object):
    def __init__(self, theta, length, mu, sigma):
        """
        Creates ARp object.
        x_(-1), x_(-2), ..., x_(-p) are supposed to be 0.
        Modelling starts at x_0.
        sigma is the standard deviation, not the variance.

        e.g.:
        >>> from ARp.ARp import ARp
        >>> x = ARp([0.5], 4, 0, 1) # 1st order ARp, len = 4, xi_t ~ N_1(0, 1)
        >>> y = ARp([0.5, -0.2], 100, 0, 5)
        """
        # TODO: incomplete assertion, need to check is the time series is stationary
        for i in theta:
            assert abs(i) < 1
        self.x = [0 for _ in range(length)]
        for i in range(length):
            if i < len(theta):
                for j in range(i):
                    self.x[i] += theta[j]*self.x[i-j-1]
            else:
                for j in range(len(theta)):
                    self.x[i] += theta[j]*self.x[i-j-1]
            self.x[i] += random.gauss(mu, sigma)

    def __getitem__(self, t):
        """
        Returns t'th realization of self.
        It is possible to retrieve 0, 1, ..., T-1 realizations.

        e.g.:
        >>> x = ARp([0.5], 4, 0, 1)
        >>> x[1]
        0.01707343513583226
        """
        try:
            return self.x[t]
        except IndexError:
            print "Time series length is %d less than %d" % (len(self.x), t)
            raise

    def __len__(self):
        """Returns length of the time series"""
        return len(self.x)

    def __str__(self):
        """Pretty-printing"""
        series_length_in_symbols = len(str(len(self)))
        return "\n" + "t" + " "*(series_length_in_symbols-1) + " | x"+"\n" + \
               "-" * (series_length_in_symbols+1) + "---" + "\n" + \
               "".join([str(i)+" "*(series_length_in_symbols-len(str(i))) + " | "
                        + str(self[i]) + '\n' for i in range(len(self))]) + "\n"

    def __repr__(self):
        return "ARp(" + str(self.x) + ")"
