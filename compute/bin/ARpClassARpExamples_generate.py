import random
import csv
from scipy.stats import norm
from math import sqrt
from ARp.ARp import ARp
from ClassARp.ClassARp import ClassARp


random.seed(1)

def main():
    # Example 1
    x = ARp([0.5], 100, 0, 1)
    y = ClassARp(x, [-float("inf"), 0])
    
    with open('../data/ARClassARp1L2_example.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(['t'] + ['x'] + ['y'])
        for i in range(len(x)):
            writer.writerow([i]+[x[i]]+[y[i]])
    
    # Example 2
    theta = [0.5]
    ksiStDeviation = 2
    x = ARp(theta, 100, 0, ksiStDeviation)
    L = 4
    lowBorders = [norm(0, sqrt(ksiStDeviation**2/(1-theta[0]**2))).ppf(i/float(L)) for i in range(L)]
    y = ClassARp(x, lowBorders)
    
    with open('../data/ARClassARp1L4ideal_example.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(['t'] + ['x'] + ['y'])
        for i in range(len(x)):
            writer.writerow([i]+[x[i]]+[y[i]])
    
    # Example 3
    x = ARp([-0.5, -0.5, 0.1, 0.2, -0.2, -0.1, -0.2, 0.2, 0.1, -0.1], 100, 0, 5)
    y = ClassARp(x, [-float("inf"), 0])
    
    with open('../data/ARClassARp10L2_example.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(['t'] + ['x'] + ['y'])
        for i in range(len(x)):
            writer.writerow([i]+[x[i]]+[y[i]])
    
    # Example 4
    x = ARp([-0.5, -0.5, 0.1, 0.2, -0.2, -0.1, -0.2, 0.2, 0.1, -0.1], 100, 0, 5)
    y = ClassARp(x, [-float("inf"), -20, -15, -10, -5, 0, 5, 10, 15, 20])
    
    with open('../data/ARClassARp10L10symmetrical_example.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(['t'] + ['x'] + ['y'])
        for i in range(len(x)):
            writer.writerow([i]+[x[i]]+[y[i]])
    
    # Example 5
    x = ARp([-0.5, -0.5, 0.1, 0.2, -0.2, -0.1, -0.2, 0.2, 0.1, -0.1], 100, 0, 5)
    y = ClassARp(x, [-float("inf"), -20, -5, 3, 15, 26, 30, 44, 55, 60])
    
    with open('../data/ARClassARp10L10asymmetrical_example.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(['t'] + ['x'] + ['y'])
        for i in range(len(x)):
            writer.writerow([i]+[x[i]]+[y[i]])

if __name__=="__main__":
    main()

