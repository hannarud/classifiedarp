from ARp.ARp import ARp


class ClassARp(object):
    def __init__(self, x, low_borders):
        """
        Creates classified ARp (ClassARp) object.
        Low borders of intervals of classification are passed as a parameters.
        Classified time series is centralized at 0 value at once.
        
        WARNING!!! Classified ARp are integers!!!

        e.g.:
        >>> from ARp.ARp import ARp
        >>> x = ARp([0.5], 4, 0, 1)
        >>> from ClassARp.ClassARp import ClassARp
        >>> y = ClassARp(x, [-float("inf"), 0])
        """
        self.low_borders = low_borders[:]
        self.y = [0 for _ in range(len(x))]
        # Classification
        for i in range(len(x)):
            j = 0
            while j < len(low_borders) and x[i] > low_borders[j]:
                j += 1
            self.y[i] = j - 1
        # Centralization
        for i in range(len(x)):
            self.y[i] = 2 * self.y[i] - (len(low_borders) - 1)

    def __getitem__(self, t):
        """
        Returns t'th realization of self.
        It is possible to retrieve 0, 1, ..., T-1 realizations.

        e.g.:
        >>> x = ClassARp(ARp([0.5], 4, 0, 1), [-float("inf"), 0])
        >>> x[1]
        0.01707343513583226
        """
        try:
            return self.y[t]
        except IndexError:
            print "Time series length is %d less than %d" % (len(self.y), t)
            raise

    def __len__(self):
        """Returns length of the time series"""
        return len(self.y)

    def borders(self):
        """Returns low borders of intervals of classification"""
        return self.low_borders[:]

    def __str__(self):
        """Pretty-printing"""
        series_length_in_symbols = len(str(len(self)))
        return "\n" + "t" + " " * (
            series_length_in_symbols - 1) + " | y" + "\n" + "-" * (
            series_length_in_symbols + 1) + "---" + "\n" + " ".join(
            [str(i) + " " * (series_length_in_symbols -
                             len(str(i))) + " | " +
             str(self[i]) + "\n" for i in range(len(self))]) + "\n"

    def __repr__(self):
        return "ClassARp(" + str(self.y) + ") by intervals " + str(self.borders())
