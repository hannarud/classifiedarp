# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Autoregressive Time Series Observed Under classification\n(classified﻿ARp)',
    'author': 'Hanna V. Rudakouskaya',
    'url': 'https://bitbucket.org/hannarud/classifiedarp',
    'download_url': 'https://bitbucket.org/hannarud/classifiedarp',
    'author_email': 'hanna.rudakouskaya@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['ARp', 'ClassARp'],
    'scripts': ['bin/ARpClassARpExamples_generate.py'],
    'name': 'classifiedARp'
}

setup(**config)
